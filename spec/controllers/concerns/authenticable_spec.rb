require 'spec_helper'

class Authentication
	include Authenticable
end

describe Authenticable do
	let(:authentication) { Authentication.new}
	subject { authentication }

	describe "#current_user" do
		before do
			@user = FactoryGirl.create :user
			request.headers["Authorization"] = @user.auth_token
			#authentication.stub(:request).and_return(request)
			allow(authentication).to receive_message_chain(:request).and_return(request)
		end

		it "retorna el usuario desde la cabecera de autorizacion" do
			expect(authentication.current_user.auth_token).to eql @user.auth_token
		end
	end

	describe "#authenticate_with_token" do
		before do
			@user = FactoryGirl.create :user
			#authentication.stub(:current_user).and_return(nil)
			#response.stub(:response_code).and_return(401)
			#response.stub(:body).and_return({"errors" => "Not authenticated"}.to_json)
			#authentication.stub(:response).and_return(response)
			allow(authentication).to receive_message_chain(:current_user).and_return(nil)
			allow(response).to receive_message_chain(:response_code).and_return(401)
			allow(response).to receive_message_chain(:body).and_return({"errors" => "Not authenticated"}.to_json)
			allow(authentication).to receive_message_chain(:response).and_return(response)
		end

		it "realiza un mensaje de error json" do
			expect(json_response[:errors]).to eql "Not authenticated"
		end

		it { should respond_with 401}
	end

	describe "#user_signed_in?" do
		context "cuando hay un usuario en session" do
			before do
				@user = FactoryGirl.create :user
				#authentication.stub(:current_user).and_return(@user)
				allow(authentication).to receive_message_chain(:current_user).and_return(@user)
			end

			it {should be_user_signed_in}
		end

		context "cuando no hay un usuario en session" do
			before do
				@user = FactoryGirl.create :user
				#authentication.stub(:current_user).and_return(nil)
				allow(authentication).to receive_message_chain(:current_user).and_return(nil)
			end

			it { should_not be_user_signed_in}
		end
	end
end
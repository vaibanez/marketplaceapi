require 'spec_helper'

RSpec.describe Api::V1::OrdersController, type: :controller do
	describe "GET #index" do
		before(:each) do
			current_user = FactoryGirl.create :user
			api_authorization_header current_user.auth_token
			4.times {FactoryGirl.create :order, user: current_user}
			get :index, user_id: current_user.id
		end

		it "retorna 4 ordenes del usuario" do
			orders_response = json_response[:orders]
			#expect(orders_response).to have(4).items
			expect(orders_response.size).to eq(4)
		end

		# it { expect(json_response).to have_key(:meta) }
		# it { expect(json_response[:meta]).to have_key(:pagination) }
		# it { expect(json_response[:meta][:pagination]).to have_key(:per_page) }
		# it { expect(json_response[:meta][:pagination]).to have_key(:total_pages) }
		# it { expect(json_response[:meta][:pagination]).to have_key(:total_objects) }

		it_behaves_like "paginated list"

		it {should respond_with 200}
	end

	describe "GET #show" do
		before(:each) do
			current_user = FactoryGirl.create :user
			api_authorization_header current_user.auth_token

			@product = FactoryGirl.create :product
			@order = FactoryGirl.create :order, user: current_user, product_ids: [@product.id]
			get :show, user_id: current_user.id, id:@order.id
		end

		it "retorna la orden de usuario ubicada por id"	do
			order_response = json_response[:order]
			expect(order_response[:id]).to eql @order.id
		end

		it "incluye el total de la orden" do
			order_response = json_response[:order]
			expect(order_response[:total]).to eql @order.total.to_s
		end

		it "incluye los productos en la orden" do
			order_response = json_response[:order]
			#expect(order_response[:products]).to have(1).item
			expect(order_response[:products].size).to eq(1)
		end

		it {should respond_with 200}
	end

	describe "POST #create" do
		before(:each) do
			current_user = FactoryGirl.create :user
			api_authorization_header current_user.auth_token

			product1 = FactoryGirl.create :product
			product2 = FactoryGirl.create :product

			#order_params = {total: 50, user_id: current_user.id, product_ids: [product1.id, product2.id]}
			order_params = { product_ids_and_quantities: [[product1.id,2], [product2.id,3]] }
			post :create, user_id: current_user.id, order: order_params
		end

		it "retorna la orden del usuario" do
			order_response = json_response[:order]
			expect(order_response[:id]).to be_present
		end

		it "incrustamos 2 productos relacionados a la orden" do
			order_response = json_response[:order]
			expect(order_response[:products].size).to eq(2)
		end

		it {should respond_with 201}
	end
end

require "spec_helper"

RSpec.describe OrderMailer, type: :mailer do
  include Rails.application.routes.url_helpers

  describe ".send_confirmation" do
  	before(:all) do
  		@order = FactoryGirl.create :order
  		@user = @order.user
  		@order_mailer = OrderMailer.send_confirmation(@order)
  	end

  	it "debe enviar al usuario la orden aprobada" do
  		#@order_mailer.should deliver_to(@user.email)
      expect(@order_mailer).to deliver_to(@user.email)
  	end

  	it "debe enviarse desde no-reply@marketplace.com" do
  		#@order_mailer.should deliver_from('no-reply@marketplace.com')
      expect(@order_mailer).to deliver_from('no-reply@marketplace.com')
  	end

  	it "debe contener el mensaje de usuario en el cuerpo del mail" do
  		#@order_mailer.should have_body_text(/Order: ##{@order.id}/)
      expect(@order_mailer).to have_body_text(/Order: ##{@order.id}/)
  	end

  	it "debe tener el correcto asunto" do
  		#@order_mailer.should have_subject(/Order Confirmation/)
      expect(@order_mailer).to have_subject(/Order Confirmation/)
  	end

  	it "debe tener el numero de productos" do
  		#@order_mailer.should have_body_text(/You ordered #{@order.products.count} products:/)
      expect(@order_mailer).to have_body_text(/You ordered #{@order.products.count} products:/)
  	end
  end
end

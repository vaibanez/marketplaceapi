require 'spec_helper'

RSpec.describe Order, type: :model do
  let(:order) { FactoryGirl.build :order}
  subject {order}

  it {should respond_to(:total)}
  it {should respond_to(:user_id)}

  it {should validate_presence_of :user_id}
  #it {should validate_presence_of :total}
  #it {should validate_numericality_of(:total).is_greater_than_or_equal_to(0)}

  it {should belong_to :user}

  it {should have_many(:placements)}
  it {should have_many(:products).through(:placements)}

  describe '#set_total!' do
    before(:each) do
      product_1 = FactoryGirl.create :product, price: 100
      product_2 = FactoryGirl.create :product, price: 85

      placement_1 = FactoryGirl.build :placement, product: product_1, quantity: 3
      placement_2 = FactoryGirl.build :placement, product: product_2, quantity: 15

      #@order = FactoryGirl.build :order, product_ids: [product_1.id, product_2.id]
      @order = FactoryGirl.build :order

      @order.placements << placement_1
      @order.placements << placement_2
    end
    
    it "retorna el monto total para pagar los productos" do
      expect{@order.set_total!}.to change{@order.total}.from(0).to(1575)
    end
  end

  describe "#build_placements_with_product_ids_and_quantities" do
    before(:each) do
      product_1 = FactoryGirl.create :product, price: 100, quantity: 5
      product_2 = FactoryGirl.create :product, price: 85, quantity: 10

      @product_ids_and_quantities = [[product_1.id,2],[product_2.id,3]]
    end

    it "construye 2 colocaciones para la orden" do
      expect{order.build_placements_with_product_ids_and_quantities(@product_ids_and_quantities)}.to change{order.placements.size}.from(0).to(2)
    end
  end

  describe "#valid?" do
    before do
      product_1 = FactoryGirl.create :product, price: 100, quantity: 5
      product_2 = FactoryGirl.create :product, price: 85, quantity: 10

      placement_1 = FactoryGirl.build :placement, product: product_1, quantity: 3
      placement_2 = FactoryGirl.build :placement, product: product_2, quantity: 15

      @order = FactoryGirl.build :order

      @order.placements << placement_1
      @order.placements << placement_2

      it "se convierte en invalido por cantidad insuficiente de productos" do
        expect(@order).to_not be_valid
      end

    end
  end
end

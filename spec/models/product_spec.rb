require 'spec_helper'

RSpec.describe Product, type: :model do
  let(:product) {FactoryGirl.build :product}
  subject {product}

  it {should respond_to(:title)}
  it {should respond_to(:price)}
  it {should respond_to(:published)}
  it {should respond_to(:user_id)}

  it {should validate_presence_of :title}
  it {should validate_presence_of :price}
  it {should validate_numericality_of(:price).is_greater_than_or_equal_to(0)}
  it {should validate_presence_of :user_id}

  it {should belong_to :user}

  it {should have_many(:placements)}
  it {should have_many(:orders).through(:placements)}

  describe ".filter_by_title" do
    before(:each) do
      @product1 = FactoryGirl.create :product, title: "A plasma TV"
      @product2 = FactoryGirl.create :product, title: "Fastest Laptop"
      @product3 = FactoryGirl.create :product, title: "CD player"
      @product4 = FactoryGirl.create :product, title: "LCD TV"
    end

    context "cuando se envia 'TV'" do
      it "retorna 2 registros" do
        #expect(Product.filter_by_title("TV")).to have(2).items
        expect(Product.filter_by_title("TV").size).to eq(2)
      end

      it "retorna los productos encontrados" do
        expect(Product.filter_by_title("TV").sort).to match_array([@product1, @product4])
      end
    end
  end

  describe ".above_or_equal_to_price" do
    before(:each) do
      @product1 = FactoryGirl.create :product, price: 100
      @product2 = FactoryGirl.create :product, price: 50
      @product3 = FactoryGirl.create :product, price: 150
      @product4 = FactoryGirl.create :product, price: 99
    end

    it "retorna los productos arriba o igual al precio" do
      expect(Product.above_or_equal_to_price(100).sort).to match_array([@product1,@product3])
    end
  end

  describe ".below_or_equal_to_price" do
    before(:each) do
      @product1 = FactoryGirl.create :product, price: 100
      @product2 = FactoryGirl.create :product, price: 50
      @product3 = FactoryGirl.create :product, price: 150
      @product4 = FactoryGirl.create :product, price: 99
    end

    it "retorna los productos debajo o igual al precio" do
      expect(Product.below_or_equal_to_price(99).sort).to match_array([@product2, @product4])
    end
  end

  describe ".recent" do
    before(:each) do
      @product1 = FactoryGirl.create :product, price: 100
      @product2 = FactoryGirl.create :product, price: 50
      @product3 = FactoryGirl.create :product, price: 150
      @product4 = FactoryGirl.create :product, price: 99

      #we will touch some products to update them
      @product2.touch
      @product3.touch
    end

    it "retorna los productos mas recientes" do
      expect(Product.recent).to match_array([@product3, @product2, @product4, @product1])
    end
  end

  describe ".search" do
    before(:each) do
      @product1 = FactoryGirl.create :product, price: 100, title: "Plasma tv"
      @product2 = FactoryGirl.create :product, price: 50, title: "Videogame console"
      @product3 = FactoryGirl.create :product, price: 150, title: "MP3"
      @product4 = FactoryGirl.create :product, price: 99, title: "Laptop"
    end

    context "cuando el titulo es 'videogame' y '100' como minimo precio se establece" do
      it "retorna un array vacio" do
        search_hash = { keyword: "videogame", min_price: 100 }
        expect(Product.search(search_hash)).to be_empty
      end
    end

    context "cuando el titulo es 'tv', '150' como precio maximo, y '50' como precio minimo se establece" do
      it "returns the product1" do
        search_hash = { keyword: "tv", min_price: 50, max_price: 150 }
        expect(Product.search(search_hash)).to match_array([@product1]) 
      end
    end

    context "cuando se envia un hash vacio" do
      it "retorna todos los productos" do
        expect(Product.search({})).to match_array([@product1, @product2, @product3, @product4])
      end
    end

    context "cuando product_ids esta presente" do
      it "retorna los productos desde los ids" do
        search_hash = { product_ids: [@product1.id, @product2.id]}
        expect(Product.search(search_hash)).to match_array([@product1, @product2])
      end
    end
  end
end

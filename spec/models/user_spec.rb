require 'spec_helper'

describe User do
  before {@user = FactoryGirl.build(:user)}

  subject {@user}

  it { should respond_to(:email)}
  it { should respond_to(:password)}
  it { should respond_to(:password_confirmation)}
  it { should have_many(:products)}
  it { should have_many(:orders)}
  it { should be_valid}

  describe 'cuando email no esta presente' do
  	before{ @user.email=""}
  	it { should_not be_valid}
  end 

  it {should validate_presence_of(:email)}
  it {should validate_uniqueness_of(:email)}
  it {should validate_confirmation_of(:password)}
  it {should allow_value('example@domain.com').for(:email)}

  it {should respond_to(:auth_token)}
  it {should validate_uniqueness_of(:auth_token)}

  describe "#generate_authentication_token!" do
    it "genera un unico token" do
      #Devise.stub(:friendly_token).and_return("auniquetoken123")
      allow(Devise).to receive_message_chain(:friendly_token).and_return("auniquetoken123")

      @user.generate_authentication_token!
      expect(@user.auth_token).to eql "auniquetoken123"
    end

    it "genera otro token cuando uno ya ha sido tomado" do
      existing_user = FactoryGirl.create(:user, auth_token: "auniquetoken123")
      @user.generate_authentication_token!
      expect(@user.auth_token).not_to eql existing_user.auth_token
    end
  end

  describe "#products association" do
    before do
      @user.save
      3.times {FactoryGirl.create :product, user: @user }
    end

    it "destruye los productos asociados en el autoborrado" do
      products = @user.products
      @user.destroy
      products.each do |product|
        expect(Product.find(product)).to raise_error Active::RecordNotFound
      end
    end
  end
end

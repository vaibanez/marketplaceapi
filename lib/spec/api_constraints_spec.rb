require 'spec_helper'

describe ApiConstraints  do
	let(:api_constraints_v1) { ApiConstraints.new(version: 1)}
	let(:api_constraints_v2) { ApiConstraints.new(version: 2, default: true)}

	describe "coinciden?" do
		it "retorna true cuando la version coincide con el 'Accept' header" do
			request = double(host: 'api.marketplace.dev', headers: {"Accept" => "application/vnd.marketplace.v1"})
			api_constraints_v1.matches?(request).should be_truthy
		end

		it "retorna la version por defecto cuando la opcion 'default' es especificada" do
			request = double(host: 'api.marketplace.dev')
			api_constraints_v2.matches?(request).should be_truthy
		end
	end
end